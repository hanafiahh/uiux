module.exports = {
    ci: {
      collect: {
        numberOfRuns: 1,
        url: ['https://www.selangor.gov.my/'],
        settings: {
          // exclude "pwa" category
          onlyCategories: [
            'performance',
            'accessibility',
            'best-practices',
            'seo',
          ],
          chromeFlags: '--no-sandbox',
          extraHeaders: JSON.stringify({
            Cookie: 'customCookie=1;foo=bar',
          }),
        },
      },
      assert: {
        assertions: {
          'categories:performance': [
            'error',
            { minScore: 0.9, aggregationMethod: 'median-run' },
          ],
          'categories:accessibility': [
            'error',
            { minScore: 1, aggregationMethod: 'pessimistic' },
          ],
          'categories:best-practices': [
            'error',
            { minScore: 1, aggregationMethod: 'pessimistic' },
          ],
          'categories:seo': [
            'error',
            { minScore: 1, aggregationMethod: 'pessimistic' },
          ],
        },
      },
      upload: {
        target: 'temporary-public-storage',
      },
    },
  }
  